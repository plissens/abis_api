
- [curl](#curl)
- [git](#git)
  - [ssh key](#ssh-key)
  - [.gitignore](#gitignore)
- [python](#python)
  - [virtual environment](#virtual-environment)
    - [scripts toelaten in powershell](#scripts-toelaten-in-powershell)
  - [libraries](#libraries)
- [source (thx john!)](#source-thx-john)

# curl

standaard tool om met een  teserver interageren, included in meeste **linux** installaties

vb.: `curl https://ttl-testme.herokuapp.com/api/users/login -X POST -v --data '{"email":"John@training.nl","password":"Telaat@2021"}' -H 'Content-Type: application/json;charset=UTF-8'`

`-v` verbose

`--data` data die meegestuurd word

`-H` header

`-X` methode

documentatie : https://curl.se/docs/manpage.html

# git

version control tool, laat toe om meerdere versies van een project bij te houden of om met meerdere personen aan éénzelfde project te werken

`git init` initialiseert een git repository in de huidige folder

`git add .` voeg (stage) alle aangepaste files in `.` (huidige folder waar de shell zich bevind) toe aan een toekomstige commit 

`git add test.py` voeg de file `test.py` toe (stage) aan een toekomstige commit

`git status` controleer welke files klaar staan (staged) om mee te nemen in volgende commit

`git commit -m "message"` maak een commit met boodschap "message"

`git clone git@bitbucket.org:plissens/abis_api.git` bestaand project clonen

`git push` aanpassinngen naar derver duwen

`git pull` aanpassingen van server halen

## ssh key

we kunnen ons authenticeren tov. een git server met een ssh key, om deze aan te maken, moeten we `ssh-keygen` uitvoeren

standaard word de key geschreven in `~/.ssh` het publieke deel van de key kunnen we printen met `cat ~\.ssh\id_rsa.pub`

hetgeen hier uitkomt moeten we op  de git server tovoegen bij **ssh keys**

## .gitignore

in de file `.gitignore` kunnen we verwijzen naar files die git niet mee op de server moet zetten meestal zijn dit zaken die specifiek aan de machine of gebruiker zijn. 
bv. alles in een folder `.venv` of een output file `*.jpg`

# python

## virtual environment

omdat het mogelijk is dat er meerdere python versies op één pc staan en dat er meerdere versies van dezelfde library gebruikt worden door verschillende projecten is het best practice om per project een virtual environment aan te maken.
meestal zetten we deze in een folder genaamd `.venv`

`python.exe -m venv .venv` virtual env aanmaken in de folder `.venv`

`.\.venv\Scripts\activate` virtual environment laden

### scripts toelaten in powershell

het kan zijn dat je het uitvoeren van scripts toe moet laten in powershell voor je de venv kan activeren.

open hiervoor powershell als administrator en voer `Set-ExecutionPolicy Unrestricted` uit

## libraries


extra libraries (die niet in de standard lib zitten) kunnen we vinden op https://pypi.org

meestal voegen we de library naam toe aan een file "requirementrs.txt", op die manier houden we een overzicht over welke libraries we nodig hebben in een project

`pip install -r requirements.txt` installeer alle libraries die in requirements.txt vermeld zijn

# source (thx john!)

```

{"email":"John@training.nl","password":"Telaat@2021"}

team01@ws94woe94:~$ curl https://ttl-testme.herokuapp.com/api/users/login -v --data '{"email":"John@training.nl","password":"Telaat@2021"}' -H 'Content-Type: application/json;charset=UTF-8'
*   Trying 3.209.172.72:443...
* TCP_NODELAY set
* Connected to ttl-testme.herokuapp.com (3.209.172.72) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-GCM-SHA256
* ALPN, server did not agree to a protocol
* Server certificate:
*  subject: CN=*.herokuapp.com
*  start date: Jun  1 00:00:00 2021 GMT
*  expire date: Jun 30 23:59:59 2022 GMT
*  subjectAltName: host "ttl-testme.herokuapp.com" matched cert's "*.herokuapp.com"
*  issuer: C=US; O=Amazon; OU=Server CA 1B; CN=Amazon
*  SSL certificate verify ok.
> POST /api/users/login HTTP/1.1
> Host: ttl-testme.herokuapp.com
> User-Agent: curl/7.68.0
> Accept: */*
> Content-Type: application/json;charset=UTF-8
> Content-Length: 53
>
* upload completely sent off: 53 out of 53 bytes
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: Cowboy
< Connection: keep-alive
< X-Powered-By: Express
< Access-Control-Allow-Origin: *
< Content-Type: application/json; charset=utf-8
< Content-Length: 378
< Etag: W/"17a-YyoSY6x1lLIyAkAakA/FzAPhpjc"
< Date: Mon, 22 Nov 2021 10:06:30 GMT
< Via: 1.1 vegur
<
* Connection #0 to host ttl-testme.herokuapp.com left intact
{"status":true,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjgsImZpcnN0TmFtZSI6ImpvaG4iLCJsYXN0TmFtZSI6InZhbiBkZSB2ZW4iLCJlbWFpbCI6IkpvaG5AdHJhaW5pbmcubmwiLCJyb2xlaWQiOm51bGwsImlhdCI6MTYzNzU3NTU5MCwiZXhwIjoxNjM3NTc5MTkwfQ.8sBvhUjazUN9bNgMcw4PmIksxBL8PBjQrrXVZy33WsE","user":{"id":28,"firstName":"john","lastName":"van de ven","email":"John@training.nl","roleid":null}}team01@ws94woe94:~$


Visual studio , installeer python 
maak project aan 
terminal opstarten 
https://git-scm.com/
PS C:\Users\Duser\John> git init


https://pypi.org/

 python.exe -m venv .venv



PS C:\Users\Duser\John> git init
Initialized empty Git repository in C:/Users/Duser/John/.git/
PS C:\Users\Duser\John>  git config --global user.email "john@training.nl"   
PS C:\Users\Duser\John> git config --global user.name "john"     
PS C:\Users\Duser\John> 

 Session contents restored from 22/11/2021 at 12:10:18 


Try the new cross-platform PowerShell https://aka.ms/pscore6

PS C:\Users\Duser\John> python.exe -m venv .venv
PS C:\Users\Duser\John> .\.venv\Scripts\activate
.\.venv\Scripts\activate : File 
C:\Users\Duser\John\.venv\Scripts\Activate.ps1 cannot be loaded because      
running scripts is disabled on this system. For more information, see        
about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.   
At line:1 char:1
+ .\.venv\Scripts\activate
+ ~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException     
    + FullyQualifiedErrorId : UnauthorizedAccess
PS C:\Users\Duser\John> .\.venv\Scripts\activate
(.venv) PS C:\Users\Duser\John> pip install -r .\requirement.txt   **** pip verwijst naar https://pypi.org/*****
Collecting requests
  Downloading requests-2.26.0-py2.py3-none-any.whl (62 kB)
     |████████████████████████████████| 62 kB 106 kB/s
Collecting certifi>=2017.4.17
  Downloading certifi-2021.10.8-py2.py3-none-any.whl (149 kB)
     |████████████████████████████████| 149 kB 2.2 MB/s
Collecting urllib3<1.27,>=1.21.1
  Downloading urllib3-1.26.7-py2.py3-none-any.whl (138 kB)
     |████████████████████████████████| 138 kB 6.4 MB/s
Collecting charset-normalizer~=2.0.0
  Downloading charset_normalizer-2.0.7-py3-none-any.whl (38 kB)
Collecting idna<4,>=2.5
  Downloading idna-3.3-py3-none-any.whl (61 kB)
     |████████████████████████████████| 61 kB 4.0 MB/s
Installing collected packages: urllib3, idna, charset-normalizer, certifi, requests
Successfully installed certifi-2021.10.8 charset-normalizer-2.0.7 idna-3.3 requests-2.26.0 urllib3-1.26.7
WARNING: You are using pip version 21.2.3; however, version 21.3.1 is available.
You should consider upgrading via the 'C:\Users\Duser\John\.venv\Scripts\python.exe -m pip install --upgrade pip' command.
(.venv) PS C:\Users\Duser\John>





import json
import requests
data = dict()
data["email"] = "John@training.nl"
data["password"] = "Telaat@2021"


requests.post ('https://ttl-testme.herokuapp.com/api/users/login', data=data)


https:/go.microsoft.com/fwlink/?LinkID=135170. Do you want to change the execution policy?
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "N"): Y
PS C:\WINDOWS\system32> ssh-keygen.exe
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\Duser/.ssh/id_rsa):
Created directory 'C:\Users\Duser/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\Duser/.ssh/id_rsa.
Your public key has been saved in C:\Users\Duser/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:hawrWrX5q9n9AUWd9/sBXlsQg2k1QTyB8CgGpJLNyVI duser@ws94woe94
The key's randomart image is:
+---[RSA 3072]----+
|    E.o   ...OO= |
|   * o o . += *o.|
|  + *   = o.o  +.|
|   o   o o . . .o|
|      o S . . o +|
|     . +   . . + |
|    o +     .   o|
|   o . + .   .  .|
|  .   o.+....    |
+----[SHA256]-----+
PS C:\WINDOWS\system32> cat C:\Users\Duser\.ssh\id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC5xAdzRhqHwvPiSYSuxGb7HyjB1vLbpk5mO8ez8WD6ierCGKDaj6+mZo/Db4RQdXMnkCNOcFK7ztUNOQkKtufslxQqsiqcCkglNS5cW7uODFnJgZiHvIabqeBv61tRX/M0/cXvOAR1HjdQLHtSzROm0dh1wWfReSvv0v+UbyHTH02+WcsVvZj5fAxDG6w0HZoSB9UnWTuTkEIjIB6gqWaP0H4ZXrv6T2PFrFCYtPbevS9TG0nL5l896VXWwhmGJedPXh8eLy9GUiZzCypBfKhp4zHVnY/1riR69mGaK7w3VGuq56yRZ4uY+Ndgi82S4TSdqXfIoL6ASx8Wkm7RdV1swXThME9JRdUyVq+KgGHVaA/2zBitrEnlfEI8kZs+ZQPoX4iAUTgZ3dfAPuQ0ZYNT8BAT+whYsZt/gJh2zxmxmJ0ltkwgL6MhdRGPeyX9SRKzRpg1APQ3sHKgaOAsDednqh4HwZmSj15xuZxo5pTPcug/5mwmv6HJ9xTqnAfoEiE= duser@ws94woe94
PS C:\WINDOWS\system32> 

PS C:\abis_api> git pull
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 244 bytes | 16.00 KiB/s, done.
From bitbucket.org:plissens/abis_api
   db0fcae..6bcf071  master     -> origin/master
Updating db0fcae..6bcf071
Fast-forward
 wulf.txt | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 wulf.txt

 ```